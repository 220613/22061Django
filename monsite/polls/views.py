from django.shortcuts import render, redirect
from .forms import EtudiantForm

def inscription_etudiant(request):
    if request.method == 'POST':
        form = EtudiantForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('confirmation')
    else:
        form = EtudiantForm()
    return render(request, 'etudiant/inscription.html', {'form': form})

def index(request):
	return HttpRessponse("merci.")
