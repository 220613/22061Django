# Create your models here.

from django.db import models


from django.db import models

class Etudiant(models.Model):
    matricule = models.CharField(max_length=10)
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    Date_naissance= models.CharField(max_length=10)
    Lieu_naissance= models.CharField(max_length=10)
    Pays_naissance= models.CharField(max_length=10)
    Département= models.CharField(max_length=10 , blank=True)
    Nationalité= models.CharField(max_length=10)
    specialite = models.CharField(max_length=50)
    STATUT_CHOICES = (("normal","Normal") ,("diplome","Diplome" ),("renvoye","Renvoye" ),("dd", "DD") ,("echange","Echange") , ("cesure","Cesure"),("abandon","Abandon"))
    VOIE_CHOICES=(("voi1","Voie 1"),("voi2","Voie 2"),("reorientation","Reorientation"))
    statut = models.CharField(max_length=50, choices=STATUT_CHOICES)
    nni = models.CharField(max_length=12)
    voie= models.CharField(max_length=50,choices=VOIE_CHOICES)
    annee_inscription= models.CharField(max_length=10)
