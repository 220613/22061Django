from django import forms
from .models import Etudiant

class EtudiantForm(forms.ModelForm):
    class Meta:
        model = Etudiant
        fields = ['matricule', 'nom', 'prenom','Date_naissance','Lieu_naissance' ,'Pays_naissance','Département','Nationalité' , 'specialite', 'statut', 'nni','voie','annee_inscription']
